# Sales history


Requirements 

The sales history schema consists of the following tables 

Product: Contains information about the products sold by the company 

   Has a unique product_id and attributes like product_name,category, 

   description,pack_size,status,product_total,effective from,effective to. 

Promotions: Contains information about promotions run by the company 

     It has a unique promo_idand attributes like promo_name,category. 

Countries: Contains the geographic areas where the comapny operates 

It has a unique country_id and country_name. 

Time_reports: Contains the month and year the sales have taken place. 

Customers:  Contains the records about the customers like a unique 

customer_id and attributes like name,dateofbirth,gender, 

email,phonenumber,address,city,pincode. 

Costs: Contains the product_id,time_id,promo_id and unit cost of the product. 

Sales: Has the information about sales made by the company.Each sale has a   

 quantity sold and total cost.It is associated with product_id,cust_id time_id and 		promo_id. 
